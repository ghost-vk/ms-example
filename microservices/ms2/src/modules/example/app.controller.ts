import { Controller, Get } from '@nestjs/common';
import { EventPattern, MessagePattern, Payload } from '@nestjs/microservices';

import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @EventPattern('some_event')
  handleSomeEvent(@Payload() payload): void {
    console.log('Получено событие! Payload: ', payload);
  }

  @MessagePattern('some_message')
  async handleSomeMessage(@Payload() payload): Promise<{ ok: true }> {
    console.log('Получено сообщение! Payload: ', payload);    

    return { ok: true }
  }
}
