import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { Logger } from 'nestjs-pino';

import { AppModule } from './app.module';
import { AppConfig } from './modules/config/types';

async function bootstrap(): Promise<void> {
  const app = await NestFactory.create(AppModule);
  const config = app.get<ConfigService<AppConfig, true>>(ConfigService);
  const logger = app.get<Logger>(Logger);
  const port = config.get('APPLICATION_PORT');

  app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.TCP,
    options: {
      host: config.get('APPLICATION_HOST'),
      port: config.get('APPLICATION_PORT'),
    },
  });

  await app.startAllMicroservices();
  logger.log(`⚡️ Microservice started successfully on port ${port}`);
}

bootstrap();
