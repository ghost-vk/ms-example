import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientProxy } from '@nestjs/microservices';
import { firstValueFrom } from 'rxjs';

import { AppConfig } from '../config/types';

@Injectable()
export class AppService implements OnModuleInit {
  constructor(
    private readonly config: ConfigService<AppConfig, true>,
    @Inject('MS_2') private readonly ms2Client: ClientProxy,
  ) {}

  getHello(): string {
    const nodeEnv = this.config.get('NODE_ENV', { infer: true });

    return `Hello nestjs-core-template! Mode: ${nodeEnv}`;
  }

  async callMicroservice2(): Promise<void> {
    this.ms2Client.emit('some_event', { service: 'telegram', payload: { data: 'some' } });

    // const o$ = this.ms2Client.send('some_message', {
    //   service: 'vector',
    //   payload: { data: 'some' },
    // });

    // o$.subscribe({
    //   next(value) {
    //     'Получено значение из ms2: ' + value;
    //   },
    //   error(err) {
    //     console.error(err);
    //   },
    //   complete() {
    //     console.log('Событие завершено');
    //   },
    // });

    const message = await firstValueFrom(this.ms2Client.send('some_message', {
      service: 'vector',
      payload: { data: 'some' },
    }));    

    console.log(message);
  }

  onModuleInit(): void {
    this.callMicroservice2();
  }
}
