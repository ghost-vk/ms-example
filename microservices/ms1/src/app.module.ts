import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';

import { AppConfigModule } from './modules/config/config.module';
import { AppController } from './modules/example/app.controller';
import { AppService } from './modules/example/app.service';
import { AppLoggerModule } from './modules/logger/appLogger.module';

@Module({
  imports: [
    AppConfigModule,
    AppLoggerModule,
    ClientsModule.register([{ name: 'MS_2', transport: Transport.TCP, options: { host: 'localhost', port: 9090 } }]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
